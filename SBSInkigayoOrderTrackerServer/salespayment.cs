//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SBSInkigayoOrderTrackerServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class salespayment
    {
        public int TblIndex { get; set; }
        public string InvoiceNumber { get; set; }
        public int PaymentType { get; set; }
        public string Description { get; set; }
        public Nullable<double> Amount { get; set; }
        public string GCNumber { get; set; }
        public string CreditMemoNumber { get; set; }
        public string CardNumber { get; set; }
        public string CardName { get; set; }
        public string FullName { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public string ApprovalCode { get; set; }
        public string BankName { get; set; }
        public string DepositTo { get; set; }
        public Nullable<System.DateTime> DateDeposited { get; set; }
        public string VerifiedBy { get; set; }
        public Nullable<System.DateTime> CardDate { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public Nullable<sbyte> IsPostDated { get; set; }
        public Nullable<sbyte> IsTerms { get; set; }
        public string Terms { get; set; }
        public string StoreCode { get; set; }
        public string DRNumber { get; set; }
        public Nullable<int> CustomerPONo { get; set; }
        public Nullable<sbyte> IsPartial { get; set; }
        public string EPBAccountNo { get; set; }
        public Nullable<System.DateTime> EPBDateDeposit { get; set; }
        public string EPBReferenceNo { get; set; }
        public string EPBCustomerBank { get; set; }
        public string EPBDepositToWhatBank { get; set; }
        public Nullable<double> FDiscountAmount { get; set; }
        public Nullable<int> CheckStatus { get; set; }
        public double BankCharges { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public string ORNumber { get; set; }
        public Nullable<bool> Migrated { get; set; }
        public Nullable<bool> CollectionType { get; set; }
        public Nullable<double> VatCharges { get; set; }
        public Nullable<bool> Uploaded { get; set; }
    }
}
