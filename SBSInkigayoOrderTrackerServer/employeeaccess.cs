//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SBSInkigayoOrderTrackerServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class employeeaccess
    {
        public int tblIndex { get; set; }
        public string Number { get; set; }
        public string AccessLevel { get; set; }
        public string Password { get; set; }
        public bool ChangePassword { get; set; }
        public bool Expired { get; set; }
        public string Comments { get; set; }
        public System.DateTime LogDateTime { get; set; }
    
        public virtual employeemaster employeemaster { get; set; }
    }
}
