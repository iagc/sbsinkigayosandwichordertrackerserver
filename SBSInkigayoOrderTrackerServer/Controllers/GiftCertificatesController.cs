﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class GiftCertificatesController : ApiController
    {
        private readonly dbmineri_sbsEntities _db = new dbmineri_sbsEntities();

        // GET: api/GiftCertificates
        public IQueryable<giftcertificate> GetGiftCertificates()
        {
            return _db.giftcertificates;
        }

        // GET: api/GiftCertificates/5
        [ResponseType(typeof(giftcertificate))]
        public IHttpActionResult GetGiftCertificate(string id)
        {
            var giftCertificate = (from g in _db.giftcertificates
                where g.GCNumber == id
                select g).SingleOrDefault();

            if (giftCertificate == null)
            {
                return Content(HttpStatusCode.BadRequest, $"GC {id} was not found");
            }

            if (giftCertificate.ValidityFrom != null && DateTime.Now.Date < giftCertificate.ValidityFrom.Value.Date)
            {
                return Content(HttpStatusCode.BadRequest, $"GC will only be valid on {giftCertificate.ValidityFrom.Value.Date.ToShortDateString()}");
            }

            if (giftCertificate.ValidityTo != null && DateTime.Now.Date > giftCertificate.ValidityTo.Value.Date)
            {
                if (giftCertificate.ValidityFrom != null)
                    return Content(HttpStatusCode.BadRequest,
                        $"GC was only valid from {giftCertificate.ValidityFrom.Value.Date.ToShortDateString()} up to {giftCertificate.ValidityTo.Value.Date.ToShortDateString()}");
            }

            if (giftCertificate.Status != 2)
            {
                return Content(HttpStatusCode.BadRequest, $"GC {giftCertificate.GCNumber} is already redeemed");
            }

            return Ok(giftCertificate);
        }

        // PUT: api/GiftCertificates/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGiftCertificate(string id)
        {

            var giftCertificate = (from g in _db.giftcertificates
                where g.GCNumber == id
                      && DateTime.Now >= g.ValidityFrom
                      && DateTime.Now <= g.ValidityTo
                      && g.Status == 2
                select g).SingleOrDefault();

            if (giftCertificate == null)
            {
                return NotFound();
            }

            giftCertificate.Status = 3;

            _db.Entry(giftCertificate).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GiftCertificateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        #region Unused code

        // POST: api/GiftCertificates
        //[ResponseType(typeof(giftcertificate))]
        //public IHttpActionResult PostGiftCertificate(giftcertificate giftCertificate)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    _db.giftcertificates.Add(giftCertificate);

        //    try
        //    {
        //        _db.SaveChanges();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (GiftCertificateExists(giftCertificate.GCNumber))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return CreatedAtRoute("DefaultApi", new { id = giftCertificate.GCNumber }, giftCertificate);
        //}

        // DELETE: api/GiftCertificates/5
        //[ResponseType(typeof(giftcertificate))]
        //public IHttpActionResult DeleteGiftCertificate(string id)
        //{
        //    var giftCertificate = _db.giftcertificates.Find(id);
        //    if (giftCertificate == null)
        //    {
        //        return NotFound();
        //    }

        //    _db.giftcertificates.Remove(giftCertificate);
        //    _db.SaveChanges();

        //    return Ok(giftCertificate);
        //}

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GiftCertificateExists(string id)
        {
            return _db.giftcertificates.Count(e => e.GCNumber == id) > 0;
        }
    }
}