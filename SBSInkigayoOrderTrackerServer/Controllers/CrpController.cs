﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer;
using SBSInkigayoOrderTrackerServer.Models;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class CrpController : ApiController
    {
        private readonly dbmineri_sbsEntities db = new dbmineri_sbsEntities();

        [Route("api/Crp/{term}")]
        public IQueryable<CrpModel> Getemployeemasters(string term)
        {
            var result = from crp in db.employeemasters
                         where crp.EmployeeDeleted.ToLower() == "false"
                         && crp.HiringType.ToLower() == "direct"
                         && crp.employeeEmploymentStatus.Trim() == ""
                         select new CrpModel
                         {
                             EmployeeNumber = crp.employeenumber,
                             EmployeeName = crp.employeeFirstname + " " + crp.employeemiddlename + " " + crp.employeeLastname
                         };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                result = result.Where(emp => emp.EmployeeNumber.Contains(term) || emp.EmployeeName.Contains(term));
            }

            return result;

        }

        // GET: api/Crp
        [Route("api/Crp")]
        public IQueryable<CrpModel> Getemployeemasters()
        {
            return from crp in db.employeemasters
                   where crp.EmployeeDeleted.ToLower() == "false"
                   && crp.HiringType.ToLower() == "direct"
                   && crp.employeeEmploymentStatus.Trim() == ""
                   select new CrpModel
                   {
                       EmployeeNumber = crp.employeenumber,
                       EmployeeName = crp.employeeFirstname + " " + crp.employeemiddlename + " " + crp.employeeLastname
                   };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool employeemasterExists(string id)
        {
            return db.employeemasters.Count(e => e.employeenumber == id) > 0;
        }
    }
}