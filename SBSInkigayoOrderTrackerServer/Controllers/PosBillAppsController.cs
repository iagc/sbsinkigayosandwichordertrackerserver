﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer;
using SBSInkigayoOrderTrackerServer.Models;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class PosBillAppsController : ApiController
    {
        private readonly dbmineri_sbsEntities _db = new dbmineri_sbsEntities();
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(HomeController));

        // GET: api/PosBillApps
        public IQueryable<posbillapp> GetPosBillApps()
        {

            var result = _db.posbillapps
                .Select(pb => new
                {
                    pb,
                    PosBilllAppDetails = pb.PosBillAppDetails.Where(pbd => pbd.PosBillAppId == pb.Id)
                })
                .AsEnumerable().Select(x => x.pb).ToList().AsQueryable();

            return result;
        }

        // GET: api/PosBillApps/5
        [ResponseType(typeof(posbillapp))]
        [Route("api/PosBillApps/{storeCode}/{hostName}/{terminalNumber}")]
        public IHttpActionResult GetPosBillApp(string storeCode, string hostName, string terminalNumber)
        {

            var posBillApp = _db.posbillapps
                .Where(p =>
                    p.TransactionDate == _db.posbillapps
                        .Where(m => m.StoreCode == storeCode && m.HostName == hostName && m.TerminalNumber == terminalNumber)
                        .OrderByDescending(m => m.TransactionDate)
                        .Select(n => n.TransactionDate).FirstOrDefault()
                    && p.StoreCode == storeCode
                    && p.HostName == hostName
                    && p.TerminalNumber == terminalNumber)
                .Select(pb => new
                {
                    pb,
                    PosBillAppDetails = pb.PosBillAppDetails.Where(pbd => pbd.PosBillAppId == pb.Id)
                })
                .AsEnumerable().Select(x => x.pb).ToList().AsQueryable().SingleOrDefault();

            if (posBillApp != null) return Ok(posBillApp);

            var pbNew = new posbillapp
            {
                EmployeeNumber = "",
                HostName = hostName,
                StoreCode = storeCode,
                TerminalNumber = terminalNumber,
                PosBillAppDetails = new List<posbillappdetail>
                    {
                        new posbillappdetail { Denomination = 1000, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 500, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 200, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 100, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 50, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 20, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 10, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 5, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 1, Qty = 0, PosBillAppId = ""},
                        new posbillappdetail { Denomination = 0.25, Qty = 0, PosBillAppId = ""}
                    }
            };

            return Ok(pbNew);

        }

        // POST: api/PosBillApps
        [ResponseType(typeof(posbillapp))]
        public IHttpActionResult PostPosBillApp([FromBody] PosBillModel posBillApp)
        {

            var posBill = MapPosBillModelToPosBillApp(posBillApp);
            _db.posbillapps.Add(posBill);

            try
            {
                _db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Log.ErrorFormat("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (DbUpdateException)
            {
                if (PosBillAppExists(posBillApp.StoreCode, posBillApp.HostName, posBillApp.TerminalNumber))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = posBill.Id }, posBillApp);
        }

        private static posbillapp MapPosBillModelToPosBillApp(PosBillModel source)
        {

            var guid = Guid.NewGuid().ToString();
            var pb = new posbillapp
            {
                Id = guid,
                TransactionDate = DateTime.Now,
                EmployeeNumber = source.EmployeeNumber,
                HostName = source.HostName,
                StoreCode = source.StoreCode,
                TerminalNumber = source.TerminalNumber
            };

            foreach (var detail in source.PosBillAppDetails)
            {
                pb.PosBillAppDetails.Add(new posbillappdetail
                {
                    Id = Guid.NewGuid().ToString(),
                    PosBillAppId = guid,
                    Denomination = detail.Denomination,
                    Qty = detail.Qty
                });
            }

            return pb;
        }

        #region Unused code

        //// PUT: api/PosBillApps/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutPosBillApp(int id, posbillapp posBillApp)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != posBillApp.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _db.Entry(posBillApp).State = EntityState.Modified;

        //    try
        //    {
        //        _db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PosBillAppExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        //// DELETE: api/PosBillApps/5
        //[ResponseType(typeof(posbillapp))]
        //public IHttpActionResult DeletePosBillApp(int id)
        //{
        //    var posBillApp = _db.posbillapps.Find(id);
        //    if (posBillApp == null)
        //    {
        //        return NotFound();
        //    }

        //    _db.posbillapps.Remove(posBillApp);
        //    _db.SaveChanges();

        //    return Ok(posBillApp);
        //}

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PosBillAppExists(string storeCode, string hostName, string terminalNumber)
        {
            return _db.posbillapps.Count(e => e.StoreCode == storeCode
                                              && e.HostName == hostName
                                              && e.TerminalNumber == terminalNumber) > 0;
        }
    }
}