﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer.Models;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class EmployeeFoodAllowanceController : ApiController
    {
        private readonly dbmineri_sbsEntities db = new dbmineri_sbsEntities();

        // GET: api/EmployeeFoodAllowance/{term}
        [Route("api/EmployeeFoodAllowance/{term}")]
        public IQueryable<EmployeeFoodAllowanceModel> Getemployeemasters(string term)
        {

            var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            var endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);


            var result = db.employeemasters
                .Where(emp => emp.EmployeeDeleted.ToLower() == "false" && emp.employeeEmploymentStatus.Trim() == "")
                .Select(emp => new EmployeeFoodAllowanceModel
                {
                    EmployeeNumber = emp.employeenumber,
                    EmployeeName = emp.employeeFirstname + " " + emp.employeemiddlename + " " + emp.employeeLastname,
                    HasAvailed = db.salessummaries.Where(x => startDate <= x.SalesTransactionDate && endDate >= x.SalesTransactionDate
                    && x.EmployeeIDForSD == emp.employeenumber).Count() > 0 ? true : false
                });

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                result = result.Where(emp => emp.EmployeeNumber.Contains(term) || emp.EmployeeName.Contains(term));
            }            

            return result;
        }

        [Route("api/EmployeeFoodAllowance")]
        public IQueryable<EmployeeFoodAllowanceModel> Getemployeemasters()
        {

            var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            var endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);


            var result = db.employeemasters
                .Where(emp => emp.EmployeeDeleted.ToLower() == "false" && emp.employeeEmploymentStatus.Trim() == "")
                .Select(emp => new EmployeeFoodAllowanceModel
                {
                    EmployeeNumber = emp.employeenumber,
                    EmployeeName = emp.employeeFirstname + " " + emp.employeemiddlename + " " + emp.employeeLastname,
                    HasAvailed = db.salessummaries.Where(x => startDate <= x.SalesTransactionDate && endDate >= x.SalesTransactionDate
                    && x.EmployeeIDForSD == emp.employeenumber).Count() > 0 ? true : false
                });

            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool employeemasterExists(string id)
        {
            return db.employeemasters.Count(e => e.employeenumber == id) > 0;
        }
    }
}