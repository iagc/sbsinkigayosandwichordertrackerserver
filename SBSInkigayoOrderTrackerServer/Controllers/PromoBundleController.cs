﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer.Models;


namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class PromoBundleController : ApiController
    {
        private readonly dbmineri_sbsEntities db = new dbmineri_sbsEntities();

        // GET: api/PromoBundle/5
        public IHttpActionResult Getpromobundleapp(int id)
        {

            var result = db.promodetailoptionapps.Where(o => o.promoAppId == id)
                .Select(po => new
                {
                    po.id,
                    bundlename = po.name,
                    po.required,
                    po.totalQty,
                    PromoItems = db.promodetailapps.Where(i => i.PromoAppId == po.promoAppId && i.PromoDetailOptionId == po.id)
                            .Join(db.t_finishedgoods, d => d.Barcode, f => f.Barcode, (d, f) => new { f.Barcode, f.FullDescription, f.Category, d.Action, d.value })
                            .Join(db.fgsellingprices.Where(fgs => fgs.DeptCode == "RET" && fgs.StoreCode == "HO"), c => c.Barcode, b => b.Barcode, (c, b) =>
                            new
                            {
                                c.Barcode,
                                c.Category,
                                c.FullDescription,
                                ShortDescription = c.FullDescription,
                                OrigPrice = b.DiscountedPrice,
                                Price = (c.Action == "price" ? c.value : (c.Action == "discount" ? b.DiscountedPrice - (b.DiscountedPrice * (c.value / 100)) : b.DiscountedPrice)),
                                
                            }).OrderBy(x => x.Barcode)
                });

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool promobundleappExists(int id)
        {
            return db.promodetailoptionapps.Count(e => e.id == id) > 0;
        }
    }
}