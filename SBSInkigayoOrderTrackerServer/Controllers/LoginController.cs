﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.WebPages;
using SBSInkigayoOrderTrackerServer.Helpers;
using SBSInkigayoOrderTrackerServer.Models;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class LoginController : ApiController
    {
        private readonly dbmineri_sbsEntities _db = new dbmineri_sbsEntities();

        // POST: api/Login/5
        [ResponseType(typeof(LoginResult))]
        public IHttpActionResult Post_Login([FromBody] AuthModel info)
        {
            var md5Pass = Utilities.CalculateMd5Hash(info.Password);

            var result = (from e in _db.employeemasters
                          join a in _db.employeeaccesses on e.employeenumber equals a.Number
                          where e.employeenumber == info.EmployeeNumber && a.Password == md5Pass
                          select new LoginResult { employeenumber = e.employeenumber, employeeFirstName = e.employeeFirstname,employeeLastname = e.employeeLastname, 
                             employeePosition = e.employeePosition, employeeStoreCode = e.employeeStoreCode, EmployeeDepartment = e.EmployeeDepartment }
            ).SingleOrDefault();

            if (result != null && !result.employeenumber.IsEmpty())
                return Ok(result);

            return NotFound();

        }
    }
}
