﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class DiscountTypesController : ApiController
    {
        private readonly dbmineri_sbsEntities db = new dbmineri_sbsEntities();

        // GET: api/DiscountTypes
        public IQueryable<discounttype> Getdiscounttypes()
        {
            return db.discounttypes;
        }

        // GET: api/DiscountTypes/PWD/5000
        [Route("api/DiscountTypes/{id}/{totalAmount}")]
        public IHttpActionResult Getdiscounttype(string id, double totalAmount)
        {
            discounttype discounttype = db.discounttypes.Find(id);
            if (discounttype == null)
            {
                return NotFound();
            }

            var result = compute(totalAmount, discounttype.Percent, (bool)discounttype.DiscountMode);

            return Ok(result);
        }

        private double compute(double subtotal, double percent, bool removeVat)
        {

            var result = subtotal;

            if (removeVat)
            {
                result /= 1.12;
            }

            var discount = result * (percent / 100);

            result = subtotal - (result - discount);

            return result;
        }

    }
}