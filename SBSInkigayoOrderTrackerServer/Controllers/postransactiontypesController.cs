﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class PosTransactionTypesController : ApiController
    {
        private readonly dbmineri_sbsEntities db = new dbmineri_sbsEntities();

        // GET: api/postransactiontypes
        public IQueryable<Models.TransactionTypeModel> Getpostransactiontypes()
        {
            return from t in db.postransactiontypes
                   select new Models.TransactionTypeModel
                   {
                       AutoCompletePayment = t.AutoCompletePayment,
                       DiscountOption = t.DiscountOption,
                       PaymentType = t.PaymentType,
                       RequiresCustomerInfo = t.RequiresCustomerInfo,
                       TransactionCode = t.Code,
                       TransactionDescription = t.Description
                   };
        }

        private bool PostransactiontypeExists(string id)
        {
            return db.postransactiontypes.Count(e => e.Code == id) > 0;
        }
    }
}