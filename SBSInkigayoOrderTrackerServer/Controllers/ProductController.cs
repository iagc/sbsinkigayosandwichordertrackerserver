﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer.Models;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class ProductController : ApiController
    {
        private readonly dbmineri_sbsEntities _db = new dbmineri_sbsEntities();

        // GET: api/Products
        public IQueryable<ProductModel> GetProducts()
        {
            return from f in _db.t_finishedgoods
                join s in _db.fgsellingprices on f.Barcode equals s.Barcode
                where s.DeptCode == "RET" 
                && s.StoreCode == "HO" 
                && f.Deleted == false 
                && f.NonTrading == false
                && f.PhaseOut == false
                && f.Hidden == false
                select new ProductModel
                {
                    Barcode = f.Barcode,
                    FullDescription = f.FullDescription,
                    ShortDescription = f.ShortDescription,
                    Price = s.DiscountedPrice,
                    OrigPrice = s.SellingPrice,
                    Category = f.Category,
                    Status = 0
                };
        }

        // GET: api/Products/5
        public IHttpActionResult GetProducts(string id)
        {

            var result = from f in _db.t_finishedgoods
                join s in _db.fgsellingprices on f.Barcode equals s.Barcode
                where s.DeptCode == "RET" && s.StoreCode == "HO" && f.Barcode == id
                select new {
                    f.Barcode,
                    f.FullDescription,
                    f.ShortDescription,
                    s.DiscountedPrice,
                    OrigPrice = s.SellingPrice,
                    f.Category
                };

            if (!result.Any())
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
