﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SBSInkigayoOrderTrackerServer;
using SBSInkigayoOrderTrackerServer.Models;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class PromoAppsController : ApiController
    {
        private readonly dbmineri_sbsEntities db = new dbmineri_sbsEntities();

        // GET: api/PromoApps
        [Route("api/PromoApps")]
        public IHttpActionResult Getpromoapp()
        {

            var result = db.promoapps
                .Where(x => x.usesCoupon == false && x.enabled == true && x.dateFrom <= DateTime.Now && x.dateTo >= DateTime.Now)
                .Select(ps => new
                {
                    PromoID = ps.id,
                    PromoDescription = ps.description,
                    DateFrom = ps.dateFrom,
                    DateTo = ps.dateTo,
                    Enabled = ps.enabled,
                    PromoItems = db.promodetailapps.Where(q => q.PromoAppId == ps.id)
                    .Join(db.t_finishedgoods, d => d.Barcode, f => f.Barcode, (d, f) => new { f.Barcode, f.FullDescription, f.Category, d.Action, d.value })
                    .Join(db.fgsellingprices.Where(fgs => fgs.DeptCode == "RET" && fgs.StoreCode == "HO"), c => c.Barcode, b => b.Barcode, (c, b) =>
                    new
                    {
                        c.Barcode,
                        c.Category,
                        c.FullDescription,
                        ShortDescription = c.FullDescription,
                        OrigPrice = b.DiscountedPrice,
                        Price = (c.Action == "price" ? c.value : (c.Action == "discount" ? b.DiscountedPrice - (b.DiscountedPrice * (c.value / 100)) : b.DiscountedPrice)),
                    })
                }).AsEnumerable().ToList().AsQueryable();

            return Ok(result);
        }

        // GET: api/PromoApps/5
        public IHttpActionResult Getpromoapp(string id)
        {

            var result = db.promoapps
                .Join(db.promovoucherapps, p => p.id, v => v.promoAppId, (p, v) =>
                new
                {
                    PromoID = p.id,
                    PromoDescription = p.name,
                    DateFrom = p.dateFrom,
                    DateTo = p.dateTo,
                    Enabled = p.enabled,
                    VoucherCode = v.voucherCode,
                    IsUsed = v.timesUsed,
                    UsesCoupon = p.usesCoupon,
                    Details = p.promodetailapps
                }).Where(x => x.VoucherCode == id)
                .Select(ps => new
                {
                    ps.PromoID,
                    ps.PromoDescription,
                    ps.DateFrom,
                    ps.DateTo,
                    ps.Enabled,
                    ps.IsUsed,
                    ps.UsesCoupon,
                    PromoItems = ps.Details.Where(q => q.PromoAppId == ps.PromoID)
                    .Join(db.t_finishedgoods, d => d.Barcode, f => f.Barcode, (d, f) => new { f.Barcode, f.FullDescription, f.Category, d.Action, d.value })
                    .Join(db.fgsellingprices.Where(fgs => fgs.DeptCode == "RET" && fgs.StoreCode == "HO"), c => c.Barcode, b => b.Barcode, (c, b) =>
                    new
                    {
                        c.Barcode,
                        c.Category,
                        c.FullDescription,
                        ShortDescription = c.FullDescription,
                        OrigPrice = b.DiscountedPrice,
                        Price = (c.Action == "price" ? c.value : (c.Action == "discount" ? b.DiscountedPrice - (b.DiscountedPrice * (c.value / 100)) : b.DiscountedPrice)),
                        ps.PromoID,
                        ps.PromoDescription,
                        Voucher = new { ps.VoucherCode, UsageLimit = 0 }
                    })
                }).AsEnumerable().ToList().AsQueryable().SingleOrDefault();

            if (result == null)
            {
                return Content(HttpStatusCode.BadRequest, $"Voucher {id} was not found");
            }

            if (result.DateFrom != null && DateTime.Now.Date < result.DateFrom.Value.Date)
            {
                return Content(HttpStatusCode.BadRequest, $"Promo {result.PromoDescription} will only be valid on {result.DateFrom.Value.Date.ToShortDateString()}");
            }

            if (result.DateTo != null && DateTime.Now.Date > result.DateTo.Value.Date)
            {
                if (result.DateFrom != null)
                    return Content(HttpStatusCode.BadRequest,
                        $"Promo {result.PromoDescription} is only valid from {result.DateFrom.Value.Date.ToShortDateString()} up to {result.DateFrom.Value.Date.ToShortDateString()}");
            }

            if (result.Enabled == false)
            {
                return Content(HttpStatusCode.BadRequest, $"Promo {result.PromoDescription} is not enabled");
            }

            if (result.IsUsed > 0)
            {
                return Content(HttpStatusCode.BadRequest, $"Voucher {id} is already used");
            }

            return Ok(result);
        }

        // PUT: api/PromoApps/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putpromoapp(int id, promoapp promoapp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != promoapp.id)
            {
                return BadRequest();
            }

            db.Entry(promoapp).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!promoappExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PromoApps
        [ResponseType(typeof(promoapp))]
        public IHttpActionResult Postpromoapp(promoapp promoapp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.promoapps.Add(promoapp);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = promoapp.id }, promoapp);
        }

        // DELETE: api/PromoApps/5
        [ResponseType(typeof(promoapp))]
        public IHttpActionResult Deletepromoapp(int id)
        {
            promoapp promoapp = db.promoapps.Find(id);
            if (promoapp == null)
            {
                return NotFound();
            }

            db.promoapps.Remove(promoapp);
            db.SaveChanges();

            return Ok(promoapp);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool promoappExists(int id)
        {
            return db.promoapps.Count(e => e.id == id) > 0;
        }
    }
}