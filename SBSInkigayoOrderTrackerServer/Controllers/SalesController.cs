﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Ajax.Utilities;
using SBSInkigayoOrderTrackerServer.Models;
using SBSInkigayoOrderTrackerServer;

namespace SBSInkigayoOrderTrackerServer.Controllers
{
    public class SalesController : ApiController
    {
        private readonly dbmineri_sbsEntities _db = new dbmineri_sbsEntities();
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(HomeController));


        // TODO: Implement Search        
        // GET: api/Sales
        [Route("api/Sales/{position}/{pageSize}/{transactionDate}/{status}")]
        public IQueryable<salessummary> GetSales(int position, int pageSize, DateTime? transactionDate, string status = "all")
        {
            var result = _db.salessummaries.AsQueryable();

            switch (status.ToLowerInvariant())
            {
                case "served":
                    result = result.Where(ss => ss.Migrated == true && ss.SalesIsVoid == 0);
                    break;
                case "unserved":
                    result = result.Where(ss => ss.Migrated == false && ss.SalesIsVoid == 0);
                    break;
                case "void":
                    result = result.Where(ss => ss.SalesIsVoid == 1);
                    break;
                default:
                    break;
            }

            if (transactionDate != null)
            {
                var start = new DateTime(transactionDate.Value.Year, transactionDate.Value.Month, transactionDate.Value.Day, 0, 0, 0);
                var end = new DateTime(transactionDate.Value.Year, transactionDate.Value.Month, transactionDate.Value.Day, 23, 59, 59);
                result = result.Where(ss => ss.SalesTransactionDate >= start && ss.SalesTransactionDate <= end);
            }

            result = result.Select(ss => new
            {
                ss,
                Detail = ss.salesdetails.Where(d => d.InvoiceNumber == ss.SalesInvoiceNumber),
                Payment = ss.salespayments.Where(d2 => d2.InvoiceNumber == ss.SalesInvoiceNumber),
                Info = ss.salescustomerinfo
            }).AsEnumerable().Select(x => x.ss).ToList().AsQueryable().Skip(position).Take(pageSize);

            return result;
        }

        // GET: api/Sales/5
        [ResponseType(typeof(salessummary))]
        public IHttpActionResult GetSales(string id)
        {
            var sale = _db.salessummaries.Find(id);
            if (sale == null)
            {
                return NotFound();
            }

            return Ok(sale);
        }

        // POST: api/Sales
        [ResponseType(typeof(SaleResult))]
        public IHttpActionResult PostSales([FromBody] SaleModel saleModel)
        {

            Log.Debug("Data Sent: " + Environment.NewLine + Newtonsoft.Json.JsonConvert.SerializeObject(saleModel));

            var sale = MapSaleModelToSalesSummary(saleModel);

            var terminal = (from t in _db.terminals
                            where t.TerminalNumber == saleModel.TerminalNumber && t.HostName == saleModel.HostName
                            select t).FirstOrDefault();

            if (terminal != null) terminal.TransactionNumber++;

            _db.Entry(terminal).State = EntityState.Modified;

            foreach (var detail in saleModel.SaleDetails)
            {
                if (detail.Voucher == null)
                    continue;

                var v = (from vch in _db.promovoucherapps where vch.voucherCode == detail.Voucher.VoucherCode select vch).FirstOrDefault();
                if (v != null)
                {
                    if (v.timesUsed == 0)
                    {
                        v.timesUsed = 1;
                        _db.Entry(v).State = EntityState.Modified;
                    }
                    else
                    {
                        throw new Exception($"Voucher {v.voucherCode} already used");
                    }
                }
            }

            _db.salessummaries.Add(sale);

            try
            {
                Log.Debug("Changes to be Saved: " + Environment.NewLine + Newtonsoft.Json.JsonConvert.SerializeObject(sale));
                _db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Log.ErrorFormat("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (DbUpdateException dbx)
            {
                if (SalesExists(sale.SalesInvoiceNumber))
                {
                    return Conflict();
                }
                else
                {
                    Log.Error("Error In Sales Controller", dbx);
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sale.SalesInvoiceNumber }, new SaleResult(sale.SalesInvoiceNumber, sale.TableNumber, sale.ChangeAmount));

        }

        private static salessummary MapSaleModelToSalesSummary(SaleModel source)
        {

            string discountPercent = string.Empty;
            string discountCode = string.Empty;
            string discountRef = string.Empty;
            string discountDescription = string.Empty;
            int guestCount = 0;
            int seniorCount = 0;

            if (source.Discounttype != null)
            {
                discountPercent = source.Discounttype.Percent.ToString();
                discountCode = source.Discounttype.Code;
                discountRef = source.Discounttype.CustomerID + "~" + source.Discounttype.CustomerName;
                discountDescription = source.Discounttype.Description;
                if (discountCode.ToLowerInvariant() == "scd"
                    || discountCode.ToLowerInvariant() == "pwd")
                {
                    guestCount = 1;
                    seniorCount = 1;
                }
            }

            string transactionTypeReferenceCode = string.Empty;

            if (source.TransactionType != null)
            {
                discountRef = source.TransactionType.TransactionCode;
                transactionTypeReferenceCode = source.TransactionType.ReferenceCode;
            }

            var sale = new salessummary
            {
                AdvancePayment = 0,
                Bagger = source.CashierEmployeeNumber,
                AppliedPoints = 0,
                CashedOut = false,
                ChangeAmount = 0,
                CustomerID = "",
                CustomerPONo = "",
                DRNumber = "",
                DatePrinted = null,
                Discount = discountPercent,
                DiscountAmount = 0,
                DiscountCode = discountCode,
                DiscountRef = discountRef,
                DiscountType = discountDescription,
                DiscountedBy = "",
                EarnedPoints = 0,
                EmployeeIDForSD = discountRef.ToLower() == "efa" ? transactionTypeReferenceCode : string.Empty,
                FDiscountAmount = source.TicketDiscount,
                GuestCount = guestCount,
                IsPointsAdded = false,
                MemberCode = "",
                MemberDiscount = "",
                Migrated = false,
                NonVatAmount = 0,
                Printed = 0,
                ReturnFlag = 0,
                SCAmount = 0,
                SICustomer = "",
                SICustomerTIN = "",
                SalesCRPNumber = source.SalesCRPNumber,
                SalesCompany = source.CompanyCode,
                SalesEmployeeNumber = source.CashierEmployeeNumber,
                SalesInvoiceNumber = source.GenerateInvoiceNumber(),
                SalesIsVoid = 0,
                SalesReturnStatus = "",
                SalesSetTransactionDate = DateTime.Now,
                SalesStoreCode = source.GetStoreCode(),
                SalesTerminalNumber = source.GetTerminalNumber(),
                SalesTransactionDate = source.GetTransactionDate(),
                SalesTransactionDescription = "",
                SalesTransactionNumber = "",
                SalesTransactionType = discountRef.ToLower() == "efa" ? "MEAL" : "",
                SalesType = source.IsPreOrder ? (sbyte?)1 : (sbyte?)0,
                SeniorCount = seniorCount,
                ServiceCharge = 0,
                TableNumber = source.AutoGenerateQueueNumber ? source.GetQueueNumber() : source.QueueNumber,
                Uploaded = false,
                VAT = 12,
            };

            var total = source.SaleDetails.Sum(item => item.Price * item.Quantity);
            var orderedDetails = source.SaleDetails.OrderBy(x => x.Barcode);
            string previousBarcode = "";
            int itemPointerCtr = 0;

            foreach (var detail in orderedDetails)
            {
                if (previousBarcode == detail.Barcode)
                {
                    itemPointerCtr += 1;
                }
                else
                {
                    itemPointerCtr = 0;
                }

                string vCode = "";

                if (detail.Voucher != null)
                {
                    vCode = detail.Voucher.VoucherCode;
                }

                sale.salesdetails.Add(new salesdetail
                {
                    Barcode = detail.Barcode,
                    FullDescription = detail.Description,
                    StoreCode = source.GetStoreCode(),
                    DiscountedPrice = detail.Price,
                    Migrated = false,
                    Uploaded = false,
                    DiscountCode = vCode,
                    DiscountAmount = detail.Quantity * (detail.OrigPrice - detail.Price),
                    DiscountRef = detail.DiscountRef,
                    DiscountType = "",
                    DiscountedBy = "",
                    CustomerPONo = "",
                    AllowCardPrice = 0,
                    AmountDiscount = 0,
                    ApprovedBy = source.CashierEmployeeNumber,
                    ApprovedByCost = "",
                    ApprovedByOver = "",
                    CASH = 0,
                    CREDITCARD = 0,
                    CRPNumber = source.CashierEmployeeNumber,
                    CommRate = 0,
                    Cost = 0,
                    Deducted = false,
                    DiscountSeries = "",
                    FDIscPerItem = total == 0 ? 0 : ((detail.Quantity * detail.Price) / total) * source.TicketDiscount,
                    IncDiscount = 0,
                    InvoiceNumber = source.GenerateInvoiceNumber(),
                    Isvoid = 0,
                    ItemPointer = itemPointerCtr,
                    Location = "SA",
                    Lotnumber = "",
                    MaxDiscount = 0,
                    MinDiscount = 0,
                    Modifier = false,
                    NetPerItem = (detail.Quantity * detail.Price) - (total == 0 ? 0 : ((detail.Quantity * detail.Price) / total) * source.TicketDiscount),
                    OrigPrice = detail.OrigPrice,
                    OverridingDiscount = 0,
                    Package = false,
                    ParentBarcode = "",
                    PercentDiscount = 0,
                    PredefinedDisc = "",
                    PriceGroup = "",
                    Quantity = detail.Quantity,
                    Remarks = "sold",
                    ReturnQty = 0,
                    ReturnsRefNumber = "",
                    SCPerItem = 0,
                    SalePrice = detail.Price,
                    SalesDate = source.GetTransactionDate(),
                    SalesTime = source.GetTransactionDate(),
                    SupplierDiscount = 0,
                    SurCharge = 0,
                    TblIndex = 0,
                    UOM = "PCS",
                    Vatable = true,
                    xSubTotal = detail.Quantity * detail.Price,
                    xSubTotal2 = 0

                });

                previousBarcode = detail.Barcode;
            }

            foreach (var payment in source.SalePayments)
            {

                var desc = "";
                switch (payment.Type)
                {
                    case PaymentType.Card:
                        desc = "Card";
                        break;
                    case PaymentType.Cash:
                        desc = "Cash";
                        break;
                    case PaymentType.Gc:
                        desc = "GC";
                        break;
                    default:
                        desc = "";
                        break;
                }

                sale.salespayments.Add(new salespayment
                {
                    Migrated = false,
                    Uploaded = false,
                    CustomerPONo = 0,
                    Description = desc,
                    StoreCode = source.GetStoreCode(),
                    FDiscountAmount = 0,
                    DRNumber = "",
                    TblIndex = 0,
                    PaymentType = (int)payment.Type,
                    InvoiceNumber = source.GenerateInvoiceNumber(),
                    Amount = payment.Amount,
                    ApprovalCode = "",
                    BankCharges = 0,
                    BankName = "",
                    CardDate = null,
                    CardName = "",
                    CardNumber = payment.GcNumber,
                    CheckStatus = 0,
                    CollectionType = false,
                    Comment1 = discountRef,
                    Comment2 = transactionTypeReferenceCode,
                    Comment3 = "",
                    CreditMemoNumber = "",
                    DateDeposited = null,
                    DepositTo = "",
                    EPBAccountNo = "",
                    EPBCustomerBank = "",
                    EPBDateDeposit = null,
                    EPBDepositToWhatBank = "",
                    EPBReferenceNo = "",
                    ExpirationDate = null,
                    FullName = "",
                    GCNumber = payment.GcNumber,
                    IsPartial = 0,
                    IsPostDated = 0,
                    IsTerms = 0,
                    ORNumber = "",
                    PaymentDate = source.GetTransactionDate(),
                    Terms = "",
                    VatCharges = 0,
                    VerifiedBy = ""
                });
            }

            sale.salescustomerinfo = new salescustomerinfo
            {
                Uploaded = 0,
                Remarks = string.Empty,
                SalesInvoiceNumber = source.GenerateInvoiceNumber(),
                CustomerID = string.Empty,
                BusinessStyle = source.CustomerBusinessStyle,
                CustomerAddress = source.CustomerAddress,
                CustomerContact = string.Empty,
                CustomerContactNumber = source.CustomerContactNumber,
                CustomerEmailAddress = string.Empty,
                CustomerName = source.CustomerName,
                CustomerPD = string.Empty,
                CustomerTIN = source.CustomerTin
            };

            var totalPayment = sale.salespayments.Sum(item => item.Amount);
            var net = sale.salesdetails.Sum(item => item.NetPerItem);

            if (!source.IsPreOrder && discountRef.ToLower() != "efa" && discountRef.ToLower() != "ft")
            {
                if (net > totalPayment)
                    throw new Exception("Total Paid is less than Total Amount Due");

                var totalCash = sale.salespayments.Where(x => x.PaymentType == (int)PaymentType.Cash).Sum(item => item.Amount);
                var totalGc = sale.salespayments.Where(x => x.PaymentType == (int)PaymentType.Gc).Sum(item => item.Amount);
                var totalCard = sale.salespayments.Where(x => x.PaymentType == (int)PaymentType.Card).Sum(item => item.Amount);

                sale.ChangeAmount = totalCash - (net - totalGc < 0 ? 0 : net - totalGc) < 0 ? 0 : totalCash - (net - totalGc < 0 ? 0 : net - totalGc);
            }

            if (discountCode.ToLowerInvariant() == "pwd" 
                || discountCode.ToLowerInvariant() == "scd")
            {
                var subtotal = sale.salesdetails.Sum(item => item.xSubTotal);
                var vatExempt = sale.salesdetails.Sum(item => item.xSubTotal) / 1.12;
                sale.DiscountAmount = subtotal - vatExempt;
                sale.NonVatAmount = vatExempt - (vatExempt * source.Discounttype.Percent / 100);
            }

            sale.Migrated = source.IsDirectToCashier;

            return sale;
        }

        [Route("api/Sales/{id}")]
        public IHttpActionResult PutSales(string id, [FromBody] SaleModel source)
        {
            var sale = (from s in _db.salessummaries
                        where s.SalesInvoiceNumber == id
                        select s).FirstOrDefault();

            if (sale == null) return StatusCode(HttpStatusCode.InternalServerError);

            var details = (from d in _db.salesdetails
                           where d.InvoiceNumber == id
                           select d);

            foreach (var item in details)
            {
                sale.salesdetails.Add(item);
            }

            AppendPaymentsToInvoice(ref sale, source);

            _db.Entry(sale).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(new SaleResult(sale.SalesInvoiceNumber, sale.TableNumber, sale.ChangeAmount));

        }


        // PUT: api/Sales/5
        [ResponseType(typeof(void))]
        [Route("api/Sales/{id}/{actionType}")]
        public IHttpActionResult PutSales(string id, int actionType = 0)
        {
            var sale = (from s in _db.salessummaries
                        where s.SalesInvoiceNumber == id
                        select s).FirstOrDefault();

            if (sale == null) return StatusCode(HttpStatusCode.InternalServerError);

            switch (actionType)
            {
                case 0:
                    sale.Migrated = true;
                    break;
                case 1:
                    sale.SalesIsVoid = 1;
                    break;
                default:
                    sale.Migrated = true;
                    break;
            }

            _db.Entry(sale).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);


        }

        private static void AppendPaymentsToInvoice(ref salessummary sale, SaleModel source)
        {
            string discountPercent = "";
            string discountCode = "";
            string discountRef = "";            
            string discountDescription = "";
            int guestCount = 0;
            int seniorCount = 0;

            if (source.Discounttype != null)
            {
                discountPercent = source.Discounttype.Percent.ToString();
                discountCode = source.Discounttype.Code;
                discountRef = source.Discounttype.CustomerID + "~" + source.Discounttype.CustomerName;
                discountDescription = source.Discounttype.Description;
                if (discountCode.ToLowerInvariant() == "scd"
                || discountCode.ToLowerInvariant() == "pwd")
                {
                    guestCount = 1;
                    seniorCount = 1;
                }
            }

            string transactionTypeReferenceCode = string.Empty;

            if (source.TransactionType != null)
            {
                discountRef = source.TransactionType.TransactionCode;
                transactionTypeReferenceCode = source.TransactionType.ReferenceCode;
            }

            sale.Discount = discountPercent;
            sale.DiscountCode = discountCode;
            sale.DiscountRef = discountRef;
            sale.DiscountType = discountDescription;
            sale.FDiscountAmount = source.TicketDiscount;
            sale.GuestCount = guestCount;
            sale.SeniorCount = seniorCount;

            if (discountCode.ToLowerInvariant() == "pwd" || discountCode.ToLowerInvariant() == "scd")
            {
                var subtotal = sale.salesdetails.Sum(item => item.xSubTotal);
                var vatExempt = sale.salesdetails.Sum(item => item.xSubTotal) / 1.12;
                sale.DiscountAmount = subtotal - vatExempt;
                sale.NonVatAmount = vatExempt - (vatExempt * source.Discounttype.Percent / 100);
            }

            var total = sale.salesdetails.Sum(item => item.SalePrice * item.Quantity);

            foreach (var detail in sale.salesdetails)
            {
                detail.FDIscPerItem = total == 0 ? 0 : ((detail.Quantity * detail.SalePrice) / total) * source.TicketDiscount;
                detail.NetPerItem = (detail.Quantity * detail.SalePrice) - (total == 0 ? 0 : ((detail.Quantity * detail.SalePrice) / total) * source.TicketDiscount);
            }

            foreach (var payment in source.SalePayments)
            {

                var desc = "";
                switch (payment.Type)
                {
                    case PaymentType.Card:
                        desc = "Card";
                        break;
                    case PaymentType.Cash:
                        desc = "Cash";
                        break;
                    case PaymentType.Gc:
                        desc = "GC";
                        break;
                    default:
                        desc = "";
                        break;
                }

                sale.salespayments.Add(new salespayment
                {
                    Migrated = false,
                    Uploaded = false,
                    CustomerPONo = 0,
                    Description = desc,
                    StoreCode = sale.SalesStoreCode,
                    FDiscountAmount = 0,
                    DRNumber = "",
                    TblIndex = 0,
                    PaymentType = (int)payment.Type,
                    InvoiceNumber = sale.SalesInvoiceNumber,
                    Amount = payment.Amount,
                    ApprovalCode = "",
                    BankCharges = 0,
                    BankName = "",
                    CardDate = null,
                    CardName = "",
                    CardNumber = payment.GcNumber,
                    CheckStatus = 0,
                    CollectionType = false,
                    Comment1 = discountRef,
                    Comment2 = transactionTypeReferenceCode,
                    Comment3 = "",
                    CreditMemoNumber = "",
                    DateDeposited = null,
                    DepositTo = "",
                    EPBAccountNo = "",
                    EPBCustomerBank = "",
                    EPBDateDeposit = null,
                    EPBDepositToWhatBank = "",
                    EPBReferenceNo = "",
                    ExpirationDate = null,
                    FullName = "",
                    GCNumber = payment.GcNumber,
                    IsPartial = 0,
                    IsPostDated = 0,
                    IsTerms = 0,
                    ORNumber = "",
                    PaymentDate = DateTime.Now,
                    Terms = "",
                    VatCharges = 0,
                    VerifiedBy = ""
                });
            }

            var totalPayment = sale.salespayments.Sum(item => item.Amount);
            var net = sale.salesdetails.Sum(item => item.NetPerItem);

            if (net > totalPayment)
                throw new Exception("Total Paid is less than Total Amount Due");

            var totalCash = sale.salespayments.Where(x => x.PaymentType == (int)PaymentType.Cash).Sum(item => item.Amount);
            var totalGc = sale.salespayments.Where(x => x.PaymentType == (int)PaymentType.Gc).Sum(item => item.Amount);
            var totalCard = sale.salespayments.Where(x => x.PaymentType == (int)PaymentType.Card).Sum(item => item.Amount);

            sale.ChangeAmount = totalCash - (net - totalGc < 0 ? 0 : net - totalGc) < 0 ? 0 : totalCash - (net - totalGc < 0 ? 0 : net - totalGc);

            sale.Migrated = true;

        }

        #region Unused code

        //// DELETE: api/Sales/5
        //[ResponseType(typeof(salessummary))]
        //public IHttpActionResult DeleteSales(string id)
        //{
        //    var sale = _db.salessummaries.Find(id);
        //    if (sale == null)
        //    {
        //        return NotFound();
        //    }

        //    _db.salessummaries.Remove(sale);
        //    _db.SaveChanges();

        //    return Ok(sale);
        //}

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SalesExists(string id)
        {
            return _db.salessummaries.Count(e => e.SalesInvoiceNumber == id) > 0;
        }
    }
}