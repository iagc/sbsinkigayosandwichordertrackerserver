﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class SaleResult
    {
        public SaleResult(string salesInvoiceNumber, int? queueNumber, double? changeAmount)
        {
            SalesInvoiceNumber = salesInvoiceNumber;
            QueueNumber = queueNumber;
            ChangeAmount = changeAmount;
        }

        public string SalesInvoiceNumber { get; set; }
        public int? QueueNumber { get; set; }

        public double? ChangeAmount { get; set; }
    }
}