﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class PromoResult
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime? dateFrom { get; set; }
        public DateTime? dateTo { get; set; }
        public bool? enabled { get; set; }
        public string Barcode { get; set; }
        public string Action { get; set; }
        public string Category { get; set; }
        public double? Price { get; set; }
        public string FullDescription { get; set; }

    }

}