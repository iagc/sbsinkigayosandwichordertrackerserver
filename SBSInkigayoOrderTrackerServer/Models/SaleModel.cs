﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Remotion.Linq.Clauses;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class SaleModel
    {

        private readonly dbmineri_sbsEntities _db = new dbmineri_sbsEntities();

        public string SalesCRPNumber { get; set; }
        public string CashierEmployeeNumber { get; set; }
        public double TicketDiscount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerContactNumber { get; set; }
        public string CustomerTin { get; set; }
        public string CustomerBusinessStyle { get; set; }
        public double ChangeAmount { get; set; }

        public string HostName { get; set; }
        public string TerminalNumber { get; set; }


        public discounttype Discounttype { get; set; }

        public List<SaleDetailModel> SaleDetails { get; set; }
        public List<SalePaymentModel> SalePayments { get; set; }
        public string CompanyCode { get; set; }

        public int QueueNumber { get; set; }
        public bool AutoGenerateQueueNumber { get; set; }
        public bool IsDirectToCashier { get; set; }
        public bool IsPreOrder { get; set; }
        public bool IsFromOrderDetail { get; set; }
        public TransactionTypeModel TransactionType { get; set; }
        internal DateTime GetTransactionDate()
        {
            return DateTime.Now;
        }

        internal string GetStoreCode()
        {

            var storeCode = (from t in _db.terminals
                where t.TerminalNumber == this.TerminalNumber && t.HostName == this.HostName
                select t.StoreCode).SingleOrDefault();

            if (storeCode != null && storeCode.Trim() == "")
            {
                throw new Exception(
                    $"No Terminal Found associated with Terminal Number: {this.TerminalNumber} and Host Name: {this.HostName}");
            }

            return storeCode;
        }

        internal string GenerateInvoiceNumber()
        {
            var transactionNumber = (from t in _db.terminals
                where t.TerminalNumber == TerminalNumber && t.HostName == HostName
                select t.TransactionNumber).SingleOrDefault();

            var storeCode = this.GetStoreCode();

            if (transactionNumber == null)
                throw new Exception(
                    $"Error generating Invoice Number. storeCode = {storeCode}, terminalNumber = {TerminalNumber}, transactionNumber = null");

            var ctrNumber = transactionNumber.Value;
            ctrNumber++;
            return $"{storeCode}-{this.TerminalNumber}-{ctrNumber:D10}";

        }

        internal int? GetQueueNumber()
        {
            var storeCode = this.GetStoreCode();
            var lastInvoiceNumber = (from s in _db.salessummaries
                where s.SalesTerminalNumber == this.TerminalNumber && s.SalesStoreCode == storeCode
                select s.SalesInvoiceNumber).Max();

            var tableNumber = (from s in _db.salessummaries
                where s.SalesInvoiceNumber == lastInvoiceNumber
                select s.TableNumber).FirstOrDefault();

            switch (tableNumber)
            {
                case 99:
                    return 1;
                default:
                    if (tableNumber == null)
                    {
                        tableNumber = 0;
                    }
                    else if (tableNumber > 99)
                    {
                        tableNumber = 0;
                    }
                    else
                    {
                        tableNumber++;
                    }                                            

                    return tableNumber;
            }
        }

        internal string GetTerminalNumber()
        {
            var terminalNumber = (from t in _db.terminals
                where t.TerminalNumber == this.TerminalNumber && t.HostName == this.HostName
                select t.TerminalNumber).SingleOrDefault();

            if (terminalNumber != null && terminalNumber.Trim() == "")
            {
                throw new Exception(
                    $"No Terminal Found associated with Terminal Number: {this.TerminalNumber} and Host Name: {this.HostName}");
            }

            return terminalNumber;

        }
    }
}