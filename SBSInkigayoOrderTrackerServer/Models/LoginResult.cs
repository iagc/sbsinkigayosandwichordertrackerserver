﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class LoginResult
    {
        public string employeenumber { get; set; }
        public string employeeFirstName { get; set; }
        public string employeeLastname { get; set; }
        public string employeePosition { get; set; }
        public string employeeStoreCode { get; set; }
        public string EmployeeDepartment { get; set; }

        public LoginResult()
        {
            
        }

        public LoginResult(string employeenumber, string employeeFirstName, string employeeLastname, string employeePosition, string employeeStoreCode, string employeeDepartment)
        {
            this.employeenumber = employeenumber;
            this.employeeFirstName = employeeFirstName;
            this.employeeLastname = employeeLastname;
            this.employeePosition = employeePosition;
            this.employeeStoreCode = employeeStoreCode;
            EmployeeDepartment = employeeDepartment;    
        }
    }
}