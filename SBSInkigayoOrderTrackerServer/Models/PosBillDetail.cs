﻿namespace SBSInkigayoOrderTrackerServer.Models
{
    public class PosBillDetail
    {
        public double Denomination { get; set; }
        public int Qty { get; set; }
    }
}