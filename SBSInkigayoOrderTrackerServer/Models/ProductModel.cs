﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class ProductModel
    {
        public ProductModel()
        {
            
        }

        public ProductModel(string barcode, string fullDescription, string shortDescription, double? price, double? origPrice, string category, int status)
        {
            Barcode = barcode;
            FullDescription = fullDescription;
            ShortDescription = shortDescription;
            Price = price;
            OrigPrice = origPrice;
            Category = category;
            Status = status;
        }

        public string Barcode { get; set; }
        public string FullDescription { get; set; }
        public string ShortDescription { get; set; }
        public double? Price { get; set; }
        public double? OrigPrice { get; set; }
        public string Category { get; set; }
        public int Status { get; set; }
    }
}