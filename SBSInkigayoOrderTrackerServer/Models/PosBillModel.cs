﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class PosBillModel
    {
        public string EmployeeNumber { get; set; }
        public string StoreCode { get; set; }
        public string HostName { get; set; }
        public string TerminalNumber { get; set; }

        public List<PosBillDetail> PosBillAppDetails { get; set; }
    }
}