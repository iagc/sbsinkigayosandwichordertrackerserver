﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class SaleDetailModel
    {
        public string Barcode { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double OrigPrice { get; set; }
        public double Discount { get; set; }
        public string DiscountRef { get; set; }
        public VoucherModel Voucher { get; set; }
    }
}