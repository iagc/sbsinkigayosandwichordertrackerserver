﻿namespace SBSInkigayoOrderTrackerServer.Models
{
    public class AuthModel
    {
        public string EmployeeNumber { get; set; }
        public string Password { get; set; }
    }
}