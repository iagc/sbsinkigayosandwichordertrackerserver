﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class EmployeeFoodAllowanceModel
    {
        public string EmployeeNumber { get; set; }
        public string EmployeeName { get; set; }
        public bool HasAvailed { get; set; }
    }
}