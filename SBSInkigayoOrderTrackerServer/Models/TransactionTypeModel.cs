﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBSInkigayoOrderTrackerServer.Models
{
    public class TransactionTypeModel
    {
        public string TransactionCode { get; set; }
        public string TransactionDescription { get; set; }
        public sbyte? RequiresCustomerInfo { get; set; }
        public sbyte? AutoCompletePayment { get; set; }
        public int? PaymentType { get; set; }
        public sbyte? DiscountOption { get; set; }
        public string ReferenceCode { get; set; }
    }
}